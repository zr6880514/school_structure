'''
Basic structure of elementary school.
'''

class SchoolRules:
    def __init__(self, study_length):
        self.study_length = study_length

class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return f"{self.name} is {self.age} years old."

class Teacher(Person):
    def __init__(self, name, age, subject, classes):
        super().__init__(name, age)
        self.subject = subject
        self.classes = classes

    def __str__(self):
        return f"{self.name} is teaching {self.subject}. His/her classes are {sorted(self.classes)}"

class Student(Person):
    def __init__(self, name, age, attending_class):
        super().__init__(name, age)
        self.attending_class = attending_class

    def __str__(self):
        return f"{self.name} is {self.attending_class} class student."

    def IsTeached(self, Teacher_instance):
        if self.attending_class in Teacher_instance.classes:
            return f"{self.name} is teached by {Teacher_instance.name}."
        return f"{self.name} is not teached by {Teacher_instance.name}."

polish_school_rules = SchoolRules(8)
adam_k = Student("Adam Kowalski", 8, "II")
piotr_n = Teacher("Piotr Nowak", 40, "History", ["II", "III", "IV", "V"])
ania_z = Teacher("Anna Zawadzka", 33, "Mathematics", ["IV", "V", "VI"])
pawel_z = Student("Paweł Zieliński", 12, "VI")
kasia_a = Student("Kasia Akacjowa", 14, "VIII")

print(kasia_a)
print(adam_k)
print(piotr_n)
print(ania_z)

print(pawel_z.IsTeached(ania_z))


